import { Visualizer } from './visualizer';
import { Environment, Point, Ant, DiscreetBrood, BroodType, ContinousBrood } from './environment';

class Simulation {
	private environment: Environment;
	private interval_id: number;
	private step_count: number;

	constructor(public width: number, public height: number, private ants: number, private brood: number) {
		this.init();
	}

	private init() {
		this.step_count = 0;
		this.environment = new Environment(this.width, this.height);
		for (let i = 0; i < this.ants; i++) {
			this.environment.add_agent(new Ant(this.environment.random_position()));
		}

		for (let i = 0; i < this.brood / 2; i++) {
			this.environment.add_brood(new DiscreetBrood(BroodType.MINUS, this.environment.random_position()));
		}
		for (let i = 0; i < this.brood / 2; i++) {
			this.environment.add_brood(new DiscreetBrood(BroodType.PLUS, this.environment.random_position()));
		}
	}

	public run() {
		this.interval_id = window.setInterval(() => {
			this.update();
		}, 0);
	}

	public pause() {
		window.clearInterval(this.interval_id);
	}

	public update() {
		this.step_count++;
		this.environment.update();
	}

	public draw(visualizer: Visualizer) {
		visualizer.fill_style('#333');
		visualizer.fill_text('Steps: ' + this.step_count, 2, 5);
		this.environment.draw(visualizer);
	}
}

export { Simulation };
