import '../scss/site.scss';

import { Simulation } from './simulation';
import { Visualizer } from './visualizer';

let visualizer;
let simulation;

const canvas = <HTMLCanvasElement>document.querySelector('#visualizer');

const start_button = <HTMLButtonElement>document.querySelector('#settings--start');
const pause_button = <HTMLButtonElement>document.querySelector('#settings--pause');

document.querySelector('#settings--pause').addEventListener('click', (e: Event) => {
	visualizer.pause();

	start_button.disabled = false;
	pause_button.disabled = true;
});

document.querySelector('#settings').addEventListener('submit', (e: Event) => {
	e.preventDefault();

	// Inital run
	if (simulation === undefined) {
		const data = new FormData(<HTMLFormElement>e.target);
		const ant_count = data.get('ant-count').toString();
		const brood_size = data.get('brood-size').toString();

		const dimension = data
			.get('dimension')
			.toString()
			.split('x');

		simulation = new Simulation(
			Number.parseInt(dimension[0]),
			Number.parseInt(dimension[1]),
			Number.parseInt(ant_count),
			Number.parseInt(brood_size)
		);

		visualizer = new Visualizer(canvas, simulation, { scale: 6 });
	}

	visualizer.render();
	simulation.run();

	start_button.disabled = true;
	pause_button.disabled = false;
});
