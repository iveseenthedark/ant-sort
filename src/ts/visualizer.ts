import { Simulation } from './simulation';

class Visualizer {
	private animation_id;
	private ctx: CanvasRenderingContext2D;

	constructor(private canvas: HTMLCanvasElement, private simulation: Simulation, private configuration = { scale: 3 }) {
		this.init();
	}

	private init() {
		const sim_width = this.simulation.width * this.configuration.scale + 1;
		const sim_height = this.simulation.height * this.configuration.scale + 1;

		const scale_x = sim_width / this.canvas.width;
		const scale_y = sim_height / this.canvas.height;

		debugger;

		this.canvas.width = sim_width;
		this.canvas.height = sim_height;
		this.ctx = this.canvas.getContext('2d');

		this.ctx.translate(0.5, 0.5);
		// this.ctx.scale(scale_x, scale_x);

		this.ctx.webkitImageSmoothingEnabled = false;
		this.ctx.mozImageSmoothingEnabled = false;
		this.ctx.imageSmoothingEnabled = false;
	}

	public render() {
		this.clear();
		this.simulation.draw(this);
		this.animation_id = window.requestAnimationFrame(() => this.render());
	}

	public pause() {
		window.cancelAnimationFrame(this.animation_id);
	}

	public clear() {
		this.fill_style('#FFF');
		this.fill_rect(0, 0, this.simulation.width, this.simulation.height);
	}

	public fill_style(c: string) {
		this.ctx.fillStyle = c;
	}

	public stroke_style(c: string) {
		this.ctx.strokeStyle = c;
	}

	public fill_rect(x: number, y: number, w: number, h: number) {
		this.ctx.fillRect(
			x * this.configuration.scale,
			y * this.configuration.scale,
			w * this.configuration.scale,
			h * this.configuration.scale
		);
	}

	public stroke_rect(x: number, y: number, w: number, h: number) {
		this.ctx.beginPath();
		this.ctx.rect(
			x * this.configuration.scale,
			y * this.configuration.scale,
			w * this.configuration.scale,
			h * this.configuration.scale
		);
		this.ctx.stroke();
	}

	public fill_circle(x: number, y: number, w: number) {
		const r = (w * this.configuration.scale) / 2;
		this.ctx.beginPath();
		this.ctx.arc(x * this.configuration.scale + r, y * this.configuration.scale + r, r, 0, 2 * Math.PI);
		this.ctx.stroke();
	}

	public fill_text(text: string, x: number, y: number, max_width?: number) {
		this.ctx.fillText(text, x * this.configuration.scale, y * this.configuration.scale);
	}
}

export { Visualizer };
