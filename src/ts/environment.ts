import { Visualizer } from './visualizer';

/**
 */
class Point {
	constructor(public x: number, public y: number) {}
}

/**
 */
class EnvironmentCell {
	ant: Agent;
	brood: Brood;
}

/**
 */
class Environment {
	private positions: Array<Array<EnvironmentCell>>;

	constructor(public width: number, public height: number) {
		this.positions = new Array(width);
		for (let i = 0; i < width; i++) {
			this.positions[i] = new Array<EnvironmentCell>(height);
			for (let j = 0; j < height; j++) {
				this.positions[i][j] = new EnvironmentCell();
			}
		}
	}

	public add_agent(agent: Ant) {
		this.positions[agent.position.x][agent.position.y].ant = agent;
	}

	public add_brood(brood: Brood) {
		this.positions[brood.position.x][brood.position.y].brood = brood;
	}

	public move_agent(agent: Ant, new_position: Point) {
		if (typeof this.get_position_state(new_position).ant === 'undefined') {
			this.positions[agent.position.x][agent.position.y].ant = undefined;
			this.positions[new_position.x][new_position.y].ant = agent;
			agent.position = new_position;
		}
	}

	public pick_up(cell: EnvironmentCell, ant: Ant) {
		if (typeof cell.brood !== 'undefined') {
			ant.pick_up(cell.brood);
			cell.brood = undefined;
		}
	}

	public put_down(cell: EnvironmentCell, ant: Ant) {
		if (typeof cell.brood === 'undefined') {
			cell.brood = ant.put_down();
			cell.brood.position = ant.position;
		}
	}

	public random_position() {
		let x = Math.floor(Math.random() * this.width);
		let y = Math.floor(Math.random() * this.height);
		return new Point(x, y);
	}

	public is_valid_position(p: Point): boolean {
		return p.x >= 0 && p.x < this.width && p.y >= 0 && p.y < this.height;
	}

	public get_position_state(position: Point): EnvironmentCell {
		return this.positions[position.x][position.y];
	}

	public update() {
		for (let i = 0; i < this.width; i++) {
			for (let j = 0; j < this.height; j++) {
				const state = this.positions[i][j];
				if (typeof state.ant !== 'undefined') {
					state.ant.update(this);
				}
			}
		}
	}

	public draw(renderer: Visualizer) {
		renderer.stroke_style('#333');
		renderer.stroke_rect(0, 0, this.width, this.height);

		for (let i = 0; i < this.width; i++) {
			for (let j = 0; j < this.height; j++) {
				const state = this.positions[i][j];
				typeof state.brood !== 'undefined' && state.brood.draw(renderer);
				typeof state.ant !== 'undefined' && state.ant.draw(renderer);
			}
		}
	}
}

/**
 */
interface Agent {
	position: Point;
	draw(renderer: Visualizer);
	update(environment: Environment);
}

/**
 */
class Ant implements Agent {
	private static readonly MOVES: Array<Point> = [
		{ x: -1, y: -1 },
		{ x: 0, y: -1 },
		{ x: 1, y: -1 },
		{ x: -1, y: 0 },
		{ x: 1, y: 0 },
		{ x: -1, y: 1 },
		{ x: 0, y: 1 },
		{ x: 1, y: 1 }
	];
	private static readonly KP: number = 0.1;
	private static readonly KM: number = 0.3;

	private memory: Array<Brood>;
	private memory_size: number;
	private memory_idx: number;
	private is_carrying: Brood;

	constructor(public position: Point) {
		this.memory = new Array<Brood>(10);
		this.memory_size = 10;
		this.memory_idx = 0;
		this.is_carrying = undefined;
	}

	public draw(renderer: Visualizer) {
		renderer.fill_style('#f00');
		renderer.fill_rect(this.position.x, this.position.y, 1, 1);
	}

	public update(environment: Environment) {
		// Move
		const move = Ant.MOVES[Math.floor(Math.random() * Ant.MOVES.length)];
		const new_position = new Point(this.position.x + move.x, this.position.y + move.y);
		if (environment.is_valid_position(new_position)) {
			environment.move_agent(this, new_position);
		}

		const state = environment.get_position_state(this.position);

		// Pick-up/put-down
		if (typeof this.is_carrying === 'undefined' && typeof state.brood !== 'undefined') {
			// Pick-up
			const pc = this.pick_up_chance(state.brood);
			if (Math.random() < pc) {
				environment.pick_up(state, this);
			}
		} else if (typeof this.is_carrying !== 'undefined' && typeof state.brood === 'undefined') {
			// Put-down
			const pc = this.put_down_chance(this.is_carrying);
			if (Math.random() < pc) {
				environment.put_down(state, this);
			}
		}

		// Update memory
		this.memory[this.memory_idx++] = state.brood;
		this.memory_idx %= this.memory_size;
	}

	public pick_up(brood: Brood) {
		this.is_carrying = brood;
	}

	public put_down() {
		const tmp = this.is_carrying;
		this.is_carrying = undefined;
		return tmp;
	}

	private pick_up_chance(brood: Brood): number {
		let f = this.memory.reduce((a, b) => b && b.compare(brood), 0);
		return Math.pow(Ant.KP / (Ant.KP + f / this.memory_size), 2);
	}

	private put_down_chance(brood: Brood): number {
		let f = this.memory.reduce((a, b) => b && b.compare(brood), 0);
		return Math.pow(f / (Ant.KM + f / this.memory_size), 2);
	}
}

/**
 */
interface Brood {
	position: Point;
	draw(renderer: Visualizer);
	compare(brood: Brood): number;
}

/**
 */
class DiscreetBrood implements Brood {
	constructor(public type: BroodType, public position: Point) {}

	public draw(renderer: Visualizer) {
		if (this.type === BroodType.MINUS) {
			renderer.fill_circle(this.position.x, this.position.y, 1);
		} else {
			renderer.stroke_rect(this.position.x, this.position.y, 1, 1);
		}
	}

	public compare(b: Brood) {
		if (this.constructor.name !== b.constructor.name) {
			return 0;
		}
		return (<DiscreetBrood>b).type === this.type ? 1 : 0;
	}
}

/**
 */
enum BroodType {
	MINUS,
	PLUS
}

/**
 */
class ContinousBrood implements Brood {
	constructor(public strength: number, public position: Point) {}

	public draw(renderer: Visualizer) {
		renderer.fill_style(`rgba(1,1,1,${this.strength}`);
		renderer.fill_rect(this.position.x, this.position.y, 1, 1);
	}

	public compare(b: Brood) {
		if (this.constructor.name !== b.constructor.name) {
			return 0;
		}
		return 1 - Math.pow((<ContinousBrood>b).strength - this.strength, 2);
	}
}

export { Point, Environment, Ant, DiscreetBrood, BroodType, ContinousBrood };
