const path = require('path');
const webpack = require('webpack');

const extract_text_plugin = require('extract-text-webpack-plugin');
const html_webpack_plugin = require('html-webpack-plugin');

module.exports = function (paths) {
    return {
        context: path.resolve(__dirname, 'src/'),
        entry: {
            site: ['./ts/index.ts']
        },
        output: {
            filename: '[name].bundle.js',
            path: path.resolve(__dirname, './build')
        },
        devtool: "source-map",
        resolve: {
            // Add '.ts' and '.tsx' as resolvable extensions.
            extensions: [".ts", ".tsx", ".js", ".json"]
        },
        module: {
            rules: [
                { test: /\.ts?$/, enforce: 'pre', loader: 'tslint-loader', options: { failOnHint: true, configuration: require('./tslint.json') } },
                { test: /\.ts?$/, loader: ['babel-loader', 'awesome-typescript-loader'] },

                { test: /\.scss$/, use: extract_text_plugin.extract({ fallback: 'style-loader', use: ['css-loader', 'sass-loader'] }) },
            ]
        },
        plugins: [
            new extract_text_plugin('site.[chunkhash].bundle.css'),
            new webpack.optimize.CommonsChunkPlugin({ name: 'site' }),
            new html_webpack_plugin({ title: 'Typescript App', template: 'index.html' }),
        ]
    }
};